export default {
  computed: {

  },
  data() {
    return {}
  },
  methods: {
    setValidationMessage() {
      let dict = {
        custom: {
          task: {
            required: "Task name is required",
          }
        }
      };
      Validator.localize("en", dict);
    },
    /**For browser history back */
    goBack() {
      history.back();
    }
  }
}
