import Vue from 'vue'
import App from './App.vue'
import {
  BootstrapVue,
  IconsPlugin
} from 'bootstrap-vue'
import VueSwal from 'vue-swal'
import VueRouter from 'vue-router'
import Routes from './routes'
import VueResource from 'vue-resource';
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);

Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(VueSwal)

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

new Vue({
  el: '#app',
  render: h => h(App),
  router: router
})
