import AddTodo from "./components/AddTodo.vue";
import TodoSection from "./components/TodoSection.vue";
import EditTodo from "./components/EditTodo.vue";

export default [{
    path: '/',
    name: 'todo-list',
    component: TodoSection,
    props: true,
    meta: {
      title: 'Todo List',
    }
  },
  {
    path: '/add-todo',
    name: '/add-todo',
    component: AddTodo,
    props: true,
    meta: {
      title: 'Add Todo',
    }
  },
  {
    name: '/edit-todos',
    path: '/edit-todo/:id',
    component: EditTodo,
    props: true,
    meta: {
      title: 'Edit Todo',
    }
  },
]
